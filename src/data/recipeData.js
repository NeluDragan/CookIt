export const data = [
  {
    id: 1,
    image: require('../images/addRecipeIcons/title.png'),
    title: 'Title',
    text: 'Enter the title of your recipe',
    key: 'name',
  },
  {
    id: 2,
    image: require('../images/addRecipeIcons/ingredients.png'),
    title: 'Ingredients',
    text: 'Enter the ingredients of your recipe',
    key: 'ingredients',
  },
  {
    id: 3,
    image: require('../images/addRecipeIcons/time.png'),
    title: 'Time',
    text: 'Enter the time it takes to make your recipe',
    key: 'preparationTime',
  },
  {
    id: 4,
    image: require('../images/addRecipeIcons/instructions.png'),
    title: 'Instructions',
    text: 'Enter the instructions for your recipe',
    key: 'instructions',
  },
  {
    id: 5,
    image: require('../images/addRecipeIcons/type.png'),
    title: 'Type',
    text: 'Enter the type of your recipe',
    key: 'type',
  },
  {
    id: 6,
    image: require('../images/addRecipeIcons/image.png'),
    title: 'Image',
    text: 'Add an image of your recipe',
    key: 'image',
  },
];
